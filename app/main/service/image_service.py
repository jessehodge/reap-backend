from app.main import db
from app.main.model.image import Image


def upload_new_image(data):
    new_image = Image(
        image_filename=data['image_filename'],
        image_description=data['image_description'],
        image_url=data['image_url'],
        user_id=data['user_id']
    )
    save_data(new_image)
    response_object = {
        'status': 'success',
        'message': 'Successfully saved.',
        'image': new_image.image_filename
    }
    return response_object, 201


def get_all_images():
    return Image.query.all()


def get_images_by_user_id(user_id):
    return Image.query.filter_by(user_id=user_id)


def save_data(data):
    db.session.add(data)
    db.session.commit()
