from flask_restplus import Namespace, fields


class ImageDto:
    api = Namespace('image', description='image related operations')
    image = api.model('image', {
        'created_at': fields.DateTime(required=False, description='created at time'),
        'image_filename': fields.String(required=False, description='image'),
        'image_description': fields.String(reqired=True, description='image description'),
        'user_id': fields.String(required=True, description='user id')
    })


class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True, description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password'),
        # 'public_id': fields.String(description='user Identifier')
    })


class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True, description='The user password '),
    })
