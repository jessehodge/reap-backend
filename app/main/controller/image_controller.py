import os

from flask import request
from flask_restplus import Resource
from flask_cors import cross_origin
from werkzeug.utils import secure_filename

from app.main.config import UPLOAD_FOLDER
from ..service.image_service import get_all_images, upload_new_image
from ..util.dto import ImageDto

api = ImageDto.api
_image = ImageDto.image


class ValidationError(ValueError):
    pass


def create_new_folder(local_dir):
    newpath = local_dir
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    return newpath


@cross_origin(origin='*')
@api.route('/')
class ImageList(Resource):

    @api.marshal_list_with(_image, envelope='data')
    @api.doc('list_of_images')
    def get(self):
        """List all images"""
        return get_all_images()

    @api.expect(_image, validate=False)
    @api.response(201, 'Image successfully uploaded.')
    @api.doc('upload a new image')
    def post(self):
        """Creates a new Image """
        try:
            file = request.files['image']
            if file:
                filename = secure_filename(file.filename)
                url = os.path.join(UPLOAD_FOLDER, filename)
                create_new_folder(UPLOAD_FOLDER)
                file.save(url)
                data = {
                    'image_filename': filename,
                    'image_url': url,
                    'image_description': request.form['image_description'],
                    'user_id': request.form['user_id']
                }
                return upload_new_image(data)
        except KeyError as e:
            raise ValidationError('Invalid image: missing ' + e.args[0])
