import datetime

from .. import db


class Image(db.Model):
    __tablename__ = 'images'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    image_url = db.Column(db.String(100), default=None, nullable=True)
    image_filename = db.Column(db.String(255), default=None, nullable=True)
    image_description = db.Column(db.String(200), default=None, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, image_filename=None, image_description=None, user_id=None, image_url=None):
        print(len(image_filename))
        self.image_filename = image_filename
        self.image_url = image_url
        self.image_description = image_description
        self.user_id = user_id

    def export_data(self):
        return {
            'image_filename': self.image_filename,
            'image_description': self.image_description,
            'image_url': self.image_url,
        }
