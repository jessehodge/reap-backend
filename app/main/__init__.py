from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_migrate import Migrate
from flask_cors import CORS

from .config import config_by_name

db = SQLAlchemy()
flask_bcrypt = Bcrypt()


def create_app(config_name):
    app = Flask(__name__)
    CORS(app)
    db.init_app(app)
    app.config.from_object(config_by_name[config_name])
    migrate = Migrate(app, db)
    migrate.init_app(app)
    with app.app_context():
        db.create_all()
    flask_bcrypt.init_app(app)

    return app
