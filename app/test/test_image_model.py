from app.main import db
from app.main.model.image import Image
from app.test.base import BaseTestCase


class TestImageModel(BaseTestCase):

    def test_image_creation(self):
        image = Image(
            image_filename='me.jpeg',
            image_description='wow',
            image_url='images/'
        )
        db.session.add(image)
        db.session.commit()
        self.assertEqual(len(Image.query.all()), 1)
        self.assertTrue(image.created_at)
